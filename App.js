/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import configureStore from './src/config/configureStore';
import RootNavigation from './src/config/rootNavigation';
import {StatusBar} from 'react-native';
import {Root} from 'native-base';
const {store, persistor} = configureStore();

const App = () => {
  return (
    <Root>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <RootNavigation />
          <StatusBar backgroundColor="#f0afeb" barStyle="dark-content" />
        </PersistGate>
      </Provider>
    </Root>
  );
};

export default App;
