import React from 'react';
import {TouchableOpacity} from 'react-native';

import {Icon, Text} from 'native-base';

const BackBtn = props => {
	return (
		<TouchableOpacity
			onPress={() => props.navigation.goBack()}
			style={{
				marginLeft: 10,
				flexDirection: 'row',
				justifyContent: 'center',
				alignItems: 'center',
			}}>
			<Icon type="FontAwesome" name="chevron-left" style={{fontSize: 15}} />
			<Text style={{fontSize: 17, marginLeft: 6, fontFamily: 'Lato-Bold'}}>
				back
			</Text>
		</TouchableOpacity>
	);
};

export default BackBtn;
