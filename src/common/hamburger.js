import React from 'react';
import {TouchableOpacity} from 'react-native';

import {Icon, Text} from 'native-base';

const Hamburger = props => {
	return (
		<TouchableOpacity
			onPress={() => props.navigation.openDrawer()}
			style={{
				marginLeft: 20,
				flexDirection: 'row',
				justifyContent: 'center',
				alignItems: 'center',
			}}>
			<Icon type="FontAwesome" name="bars" style={{fontSize: 18}} />
		</TouchableOpacity>
	);
};

export default Hamburger;
