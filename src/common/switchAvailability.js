import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {Switch} from 'react-native-switch';
import {Icon} from 'native-base';
const SwitchAvailability = props => {
	const [switchStatus, toggleSwitch] = useState(false);

	return (
		<View
			style={{
				right: 10,
			}}>
			<Switch
				value={switchStatus}
				onValueChange={val => toggleSwitch(val)}
				disabled={false}
				activeText="On"
				inActiveText="Off"
				renderInsideCircle={() => (
					<Icon
						type="FontAwesome5"
						name={switchStatus ? 'tools' : 'power-off'}
						style={{fontSize: 18}}
					/>
				)}
				circleSize={30}
				backgroundActive={switchStatus ? 'green' : 'grey'}
				backgroundInactive={'red'}
				circleActiveColor={'white'}
				circleInActiveColor={'white'}
				changeValueImmediately={true}
				changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
				innerCircleStyle={{
					alignItems: 'center',
					justifyContent: 'center',
				}} // style for inner animated circle for what you (may) be rendering inside the circle
				outerCircleStyle={{}} // style for outer animated circle
				renderActiveText={false}
				renderInActiveText={false}
			/>
		</View>
	);
};

export default SwitchAvailability;
