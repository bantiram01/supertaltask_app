import React from 'react';
import {View, Text} from 'react-native';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LandingPage from '../screens/landing';
import Board from '../screens/board';
import AddList from '../screens/addList';
import AddCard from '../screens/addCard';

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: LandingPage,
    },
    Board: {
      screen: Board,
    },
    AddList: {
      screen: AddList,
    },
    AddCard: AddCard,
  },
  {
    initialRouteName: 'Home',
    /* The header config from HomeScreen is now here */
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#f0afeb',
      },
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    },
  },
);

export default createAppContainer(AppNavigator);
