import * as TYPE from './types';

const initialState = {
  lists: [
    {
      name: 'Todo',
      id: 1,
      cards: [],
    },
    {
      name: 'In progress',
      id: 2,
      cards: [],
    },
  ],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case TYPE.CREATE_NEW_LIST:
      state.lists = [...state.lists, action.data];
      return {...state};
    case TYPE.DELETE_LIST:
      var {id} = action.data,
        index = state.lists.findIndex(data => data.id === id);
      state.lists.splice(index, 1);

      return {...state};
    case TYPE.ADD_CARD_TO_LIST:
      var {listId} = action.data,
        _index = state.lists.findIndex(data => data.id === listId);

      if (state.lists[_index].cards) {
        state.lists[_index].cards = [...state.lists[_index].cards, action.data];
      }
      return {...state};

    case TYPE.CHANGE_CARD_STATUS:
      let {card, listIdFrom, moveToList} = action.data;
      console.log(action.data, 'action.data');
      card.listId = moveToList;

      let indexToMoveIn = state.lists.findIndex(data => moveToList === data.id);

      state.lists[indexToMoveIn].cards.push(card);

      let indexToRemoveFrom = state.lists.findIndex(
          data => listIdFrom === data.id,
        ),
        cardIndexToBeRemoved = state.lists[indexToRemoveFrom].cards.findIndex(
          data => data.id === card.id,
        );

      state.lists[indexToRemoveFrom].cards.splice(cardIndexToBeRemoved, 1);

      return {...state};

    case TYPE.DELETE_CARD:
      var {listId, id} = action.data,
        listIndexToRemoveFrom = state.lists.findIndex(
          data => listId === data.id,
        ),
        _cardIndexToBeRemoved = state.lists[
          listIndexToRemoveFrom
        ].cards.findIndex(data => data.id === id);

      state.lists[listIndexToRemoveFrom].cards.splice(_cardIndexToBeRemoved, 1);

      return {...state};

    case TYPE.UPDATE_LIST_NAME:
      let {listName, listIdToBeUpdated} = action.data,
        listIndexToRenamed = state.lists.findIndex(
          data => listIdToBeUpdated === data.id,
        );

      state.lists[listIndexToRenamed].name = listName;

      return {...state};

    default:
      return state;
  }
}
