import React, {Component} from 'react';
import {TextField} from 'react-native-material-textfield';
import LinearGradient from 'react-native-linear-gradient';
import {StyleSheet, View} from 'react-native';
import {Button, Text} from 'native-base';
import {Dropdown} from 'react-native-material-dropdown';
import {connect} from 'react-redux';
import {addCardToList} from '../../redux/app/actions';

import {bindActionCreators} from 'redux';
class AddCard extends Component {
  state = {
    statusVal: '',
  };
  descRef = React.createRef();
  assignedRef = React.createRef();
  getListNames = lists => {
    lists.map((data, index) => {
      data.value = data.name;
      return data;
    });

    return lists;
  };

  addCard = () => {
    let {current: cardDescription} = this.descRef,
      {current: assignedUsername} = this.descRef,
      {lists} = this.props.app,
      {statusVal} = this.state,
      index = lists.findIndex(data => data.name === statusVal);
    let addCardObj = {
      cardDescription: cardDescription.value(),
      assignedUsername: assignedUsername.value(),
      listId: lists[index].id,
      id: Math.random(),
    };
    this.props.addCardToList(addCardObj);
    alert('success!');
    cardDescription.clear();
    assignedUsername.clear();
  };

  render() {
    let {lists} = this.props.app;
    let data = this.getListNames(lists);
    return (
      <LinearGradient
        colors={['#f0afeb', '#9eb4e6']}
        style={styles.linearGradient}>
        <View style={styles.container}>
          <Text style={styles.heading}>Add a Card</Text>
          <TextField
            label="Description"
            multiline
            ref={this.descRef}
            inputContainerStyle={{width: '100%', marginTop: '2%'}}
          />
          <TextField
            label="Assigned To"
            ref={this.assignedRef}
            inputContainerStyle={{width: '100%'}}
          />
          <Dropdown
            label="Choose One"
            onChangeText={e =>
              this.setState({
                statusVal: e,
              })
            }
            data={data}
          />
          <View style={styles.bottom}>
            <Button
              full
              dark
              rounded
              block
              style={styles.button}
              onPress={this.addCard}>
              <Text>Submit</Text>
            </Button>
          </View>
        </View>
      </LinearGradient>
    );
  }
}

var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  image: {
    marginTop: 50,
  },
  heading: {
    marginTop: 40,
  },
  text: {
    marginHorizontal: 8,
    marginVertical: 10,
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 36,
  },
});

const mapStateToProps = state => {
  return {
    app: state.app,
  };
};

const mapDispatchToProps = dispatch => ({
  addCardToList: bindActionCreators(addCardToList, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddCard);
