import React, {Component} from 'react';
import {TextField} from 'react-native-material-textfield';
import LinearGradient from 'react-native-linear-gradient';
import {StyleSheet, View} from 'react-native';
import {Button, Text, Toast} from 'native-base';
import {connect} from 'react-redux';
import _ from 'underscore';
import {bindActionCreators} from 'redux';
import {createNewList} from '../../redux/app/actions';

class AddList extends Component {
  fieldRef = React.createRef();

  createNewList = () => {
    let {current: field} = this.fieldRef;

    let newListObj = {
      name: field.value(),
      id: _.random(3, 1000),
      cards: [],
    };

    this.props.createNewList(newListObj);
    alert('Success!');
    field.clear();
  };

  render() {
    return (
      <LinearGradient
        colors={['#f0afeb', '#9eb4e6']}
        style={styles.linearGradient}>
        <View style={styles.container}>
          <Text style={styles.heading}>Create a new list</Text>
          <TextField
            label="Enter a list name"
            onSubmitEditing={this.createNewList}
            ref={this.fieldRef}
            inputContainerStyle={{width: '100%', marginTop: '2%'}}
          />
          <View style={styles.bottom}>
            <Button
              full
              dark
              rounded
              block
              style={styles.button}
              onPress={this.createNewList}>
              <Text>Submit</Text>
            </Button>
          </View>
        </View>
      </LinearGradient>
    );
  }
}

var styles = StyleSheet.create({
  linearGradient: {
    flex: 1,
  },
  container: {
    flex: 1,
    padding: 20,
  },
  image: {
    marginTop: 50,
  },
  heading: {
    marginTop: 40,
  },
  text: {
    marginHorizontal: 8,
    marginVertical: 10,
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 36,
  },
});

const mapDispatchToProps = dispatch => ({
  createNewList: bindActionCreators(createNewList, dispatch),
});

export default connect(
  null,
  mapDispatchToProps,
)(AddList);
