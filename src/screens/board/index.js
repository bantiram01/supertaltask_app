import React, {Component} from 'react';
import {FlatList, View, Image} from 'react-native';
import {Container, Tab, Tabs, Text, Button, ScrollableTab} from 'native-base';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {changeCardStatus, deleteCard} from '../../redux/app/actions';
import {Dropdown} from 'react-native-material-dropdown';

class SuperTalBoard extends React.PureComponent {
  deleteCard(item) {
    this.props.deleteCard(item);
  }

  getListNames = lists => {
    lists.map((data, index) => {
      data.value = data.name;
      return data;
    });

    return lists;
  };

  changeCardStatus = (item, id) => {
    let changeStatusObj = {
      card: item,
      listIdFrom: item.listId,
      moveToList: id,
    };
    this.props.changeCardStatus(changeStatusObj);
  };

  renderTabs(lists) {
    let dropdownData = this.getListNames(lists);
    return (
      lists &&
      lists.length > 0 &&
      lists.map((data, index) => {
        return (
          <Tab
            heading={data.name}
            tabStyle={{backgroundColor: '#f0afeb'}}
            textStyle={{color: '#000000'}}
            activeTabStyle={{backgroundColor: '#f0afeb'}}
            activeTextStyle={{color: '#000000'}}>
            <FlatList
              keyExtractor={(item, i) => i}
              extraData={this.props}
              data={data.cards}
              renderItem={({item}) => (
                <View
                  style={{
                    flex: 1,
                    padding: 10,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <View style={{flex: 0.6}}>
                    <Image
                      source={{
                        uri:
                          'https://www.tm-town.com/assets/default_male600x600-79218392a28f78af249216e097aaf683.png',
                      }}
                      style={{height: 40, width: 40}}
                    />
                    <Text style={{fontSize: 13}}>{item.assignedUsername}</Text>
                  </View>
                  <View style={{flex: 1, justifyContent: 'flex-start'}}>
                    <Text style={{fontSize: 14}}>{item.cardDescription}</Text>
                  </View>
                  <View style={{flex: 0.5, justifyContent: 'flex-start'}}>
                    <Dropdown
                      label="Status"
                      onChangeText={(e, index, data) => {
                        this.changeCardStatus(item, data[index].id);
                      }}
                      data={dropdownData}
                    />
                    <Button
                      rounded
                      danger
                      small
                      onPress={() => this.deleteCard(item)}>
                      <Text style={{fontSize: 12}}>Delete</Text>
                    </Button>
                  </View>
                </View>
              )}
              ItemSeparatorComponent={this.renderSeparator}
            />
          </Tab>
        );
      })
    );
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#000',
        }}
      />
    );
  };

  render() {
    let {lists} = this.props.app;
    return (
      <Container>
        <Tabs renderTabBar={() => <ScrollableTab />}>
          {this.renderTabs(lists)}
        </Tabs>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    app: state.app,
  };
};

const mapDispatchToProps = dispatch => ({
  changeCardStatus: bindActionCreators(changeCardStatus, dispatch),
  deleteCard: bindActionCreators(deleteCard, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SuperTalBoard);
