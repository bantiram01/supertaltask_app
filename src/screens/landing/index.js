import React from 'react';
import {View, StyleSheet} from 'react-native';
import {Button, Text} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';

const LandingPage = props => {
  return (
    <LinearGradient
      colors={['#f0afeb', '#9eb4e6']}
      style={styles.linearGradient}>
      <View style={{flex: 100}}>
        <View style={styles.welcomeContainer}>
          <View style={styles.welcomeTextContainer}>
            <Text>Welcome</Text>
          </View>
        </View>
        <View style={{flex: 40}}>
          <View
            style={{
              flexDirection: 'column',
              justifyContent: 'center',
              marginTop: 5,
            }}>
            <Text
              style={{
                color: '#2253b8',
                fontSize: 22,
                textAlign: 'center',
              }}>
              Supertal assignment - Banti Ram
            </Text>
            <Text
              style={{
                marginTop: 15,
                fontSize: 16,
                textAlign: 'center',
              }}>
              18th May - 2020
            </Text>
            <Text
              style={{
                marginTop: 10,
                fontSize: 12,
                textAlign: 'center',
              }}>
              Click on the buttons below to proceed
            </Text>
            <View
              style={{
                flex: 100,
                marginTop: 20,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                padding: 40,
              }}>
              <View style={{flex: 45}}>
                <Button
                  full
                  rounded
                  dark
                  onPress={() => props.navigation.navigate('AddList')}>
                  <Text style={{color: 'white'}}>Add List</Text>
                </Button>
              </View>
              <View style={{flex: 10}} />
              <View style={{flex: 45}}>
                <Button
                  full
                  rounded
                  dark
                  onPress={() => props.navigation.navigate('AddCard')}>
                  <Text style={{color: 'white'}}>Add Card</Text>
                </Button>
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            flex: 10,
            padding: 20,
          }}>
          <Button
            full
            rounded
            dark
            onPress={() => props.navigation.navigate('Board')}>
            <Text style={{color: 'white'}}>Proceed towards board</Text>
          </Button>
        </View>
      </View>
    </LinearGradient>
  );
};

var styles = StyleSheet.create({
  linearGradient: {
    flex: 100,
  },
  welcomeContainer: {
    flex: 30,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  welcomeTextContainer: {justifyContent: 'center', alignItems: 'center'},
  buttonText: {
    fontSize: 18,
    fontFamily: 'Gill Sans',
    textAlign: 'center',
    margin: 10,
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
});

export default LandingPage;
